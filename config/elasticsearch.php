<?php
return [
    'ssl_verification' => env('ELASTICSEARCH_SSL_VERIFICATION', false),
    'host' => env('ELASTICSEARCH_HOST', 'localhost'),
    'port' => env('ELASTICSEARCH_PORT', 9200),
];