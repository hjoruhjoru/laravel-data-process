<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Redis;
use App\Models\Employee;
#use Illuminate\Support\Facades\Cache;

class EmployeeCacheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limit = 200;
        $page = 0;
        $employees = false;
        while ($employees === false || $employees->count() == 200) {
            $employees = Employee::select('emp_no', 'birth_date', 'first_name', 'last_name', 'gender', 'hire_date')
                ->with(['deptEmp', 'deptEmp.department.managers', 'salaries', 'titles'])    
                ->skip($page * $limit)
                ->take($limit)
                ->get();
            foreach ($employees as $employee) {
                $employee = $employee->toArray();
                Redis::set('employees:' . $employee['emp_no'], json_encode($employee));
            }
            $page++;
            break;
        }
    }
}
