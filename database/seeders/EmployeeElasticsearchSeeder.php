<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Helper\ElasticsearchHelper;
use App\Models\Employee;

class EmployeeElasticsearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $esClient = ElasticsearchHelper::getClient(); 
        
        $limit = 200;
        $page = 0;
        $employees = false;
        while ($employees === false || $employees->count() == 200) {
            $employees = Employee::select('emp_no', 'birth_date', 'first_name', 'last_name', 'gender', 'hire_date')
                ->with(['deptEmp', 'deptEmp.department.managers', 'salaries', 'titles'])    
                ->skip($page * $limit)
                ->take($limit)
                ->get();
            foreach ($employees as $employee) {
                $employee = $employee->toArray();
                $params = [
                    'index' => 'employees',
                    'id' => $employee['emp_no'],
                    'body' => $employee
                ];
                $response = $esClient->index($params);
            }
            $page++;
        }
    }
}
