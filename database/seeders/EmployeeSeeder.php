<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbUsername = Config('database.connections.mysql.username');
        $dbPassword = Config('database.connections.mysql.password');
        $dbHost = Config('database.connections.mysql.host');
        $dbDatabase = Config('database.connections.mysql.database');
        $dbFilePath = getcwd() . '/database/sql_file/';
        $files = scandir($dbFilePath);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $filePath = $dbFilePath . $file;
            exec("mysql --user={$dbUsername} --password={$dbPassword} --host={$dbHost} --database {$dbDatabase} < $filePath");
        }
    }
}