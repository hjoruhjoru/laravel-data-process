<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeptManager extends Model
{
    use HasFactory;

    protected $table = 'dept_manager';

    protected $hidden = ['created_at', 'updated_at'];
}
