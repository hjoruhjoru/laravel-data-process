<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeptEmp extends Model
{
    use HasFactory;

    protected $table = 'dept_emp';

    protected $hidden = ['created_at', 'updated_at'];

    public function department()
    {
        return $this->hasOne('App\Models\Department', 'dept_no','dept_no');
    }
}
