<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    public function managers()
    {
        return $this->hasMany('App\Models\DeptManager', 'dept_no','dept_no');
    }
}
