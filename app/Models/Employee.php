<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

    public function deptEmp()
    {
        return $this->hasOne('App\Models\DeptEmp', 'emp_no','emp_no');
    }

    public function salaries()
    {
        return $this->hasMany('App\Models\Salary', 'emp_no','emp_no');
    }

    public function titles()
    {
        return $this->hasMany('App\Models\Title', 'emp_no','emp_no');
    }

    public static function getByEmpNo($empNo)
    {
        return self::with(['deptEmp', 'deptEmp.department.managers', 'salaries', 'titles'])
                ->where('emp_no', $empNo)
                ->first();       
    }

    public static function getByDept($deptNo)
    {
        $limit = 200;
        $page = 0;
        $employees = false;
        $result = [];
        while ($employees === false || $employees->count() == 200) {
            $employees = self::with(['deptEmp' => function($q) use ($deptNo) {
                    $q->where('dept_emp.dept_no', '=', $deptNo);
                }, 'deptEmp.department.managers', 'salaries', 'titles'])    
                ->skip($page * $limit)
                ->take($limit)
                ->get();
            $result += $employees->toArray();
            $page++;
        }
        return $result;
    }
}
