<?php

namespace App\Helper;

use Elastic\Elasticsearch\ClientBuilder;
use Elastic\Elasticsearch\Exception\ClientResponseException;

class ElasticsearchHelper
{
    public static function getClient()
    {
        return ClientBuilder::create()
            ->setSSLVerification(config('elasticsearch.ssl_verification'))
            ->setHosts([config('elasticsearch.host') . ':' . config('elasticsearch.port')])
            ->build();
    }

    public static function getEmployeeByNo($empNo)
    {
        $client = self::getClient();
        $params = [
            'index' => 'employees',
            'id' => $empNo
        ];
        try {
            $response = $client->get($params)->asArray();
            if (isset($response['_source'])) {
                $response = $response['_source'];
            }
        } catch (ClientResponseException $e) {
            return json_decode('{}');
        }
        return $response;
    }
    
    public static function getEmployeeByDeptNo($deptNo)
    {
        $client = self::getClient();
        $params = [
            'index' => 'employees',
            'size' => 10000,
            'body'  => [
                'query' => [
                    'bool' => [ 
                        'filter' => [
                           [ 
                                'term' =>  [
                                    'dept_emp.dept_no' => $deptNo
                                ] 
                           ]
                        ]
                    ]
                ]
            ]
        ];
        try {
            $response = $client->search($params)->asArray();
            $response = $response['hits'];
            if (isset($response['hits'])) {
                $response = $response['hits'];
            }
        } catch (ClientResponseException $e) {
            return json_decode('{}');
        }
        return $response;
    }

    public static function getEmployees()
    {
        $client = self::getClient();
        $params = [
            'index' => 'employees',
            'size' => 10000,
            'body' => [
            ]
        ];
        try {
            $response = $client->search($params)->asArray();
        } catch (ClientResponseException $e) {
            return json_decode('{}');
        }
        return $response;
    }
}