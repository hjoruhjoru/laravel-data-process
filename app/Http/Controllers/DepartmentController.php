<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Employee;
use App\Helper\ElasticsearchHelper;

class DepartmentController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $emp_no
     * @return \Illuminate\Http\Response
     */
    public function listEmployees(Request $request, $deptNo)
    {
        $start = Carbon::now()->getTimestampMs();
        $employee = ($request->query('source') == 'db') ? Employee::getByDept($deptNo) : ElasticsearchHelper::getEmployeeByDeptNo($deptNo); 
        $end = Carbon::now()->getTimestampMs();
        return response()->json(['excute_time(ms)' => $end - $start, 'data' => $employee]);
    }
}
