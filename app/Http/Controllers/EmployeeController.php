<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use App\Models\Employee;
use App\Helper\ElasticsearchHelper;

class EmployeeController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $emp_no
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $empNo)
    {
        $start = Carbon::now()->getTimestampMs();
        $employee = null;
        if ($request->query('source') == 'db') {
            $employee = Employee::getByEmpNo($empNo);
        } else if ($request->query('source') == 'es') {
            $employee = ElasticsearchHelper::getEmployeeByNo($empNo);
        } else {
            $employee = json_decode(Redis::get('employees:' . $empNo));
        }
        $end = Carbon::now()->getTimestampMs();
        return response()->json(['excute_time(ms)' => $end - $start, 'data' => $employee]);
    }

    public function showAll(Request $request)
    {
        $start = Carbon::now()->getTimestampMs();
        $employee = null;
        if ($request->query('source') == 'db') { 
            $employee = Employee::with(['deptEmp', 'deptEmp.department.managers', 'salaries', 'titles'])
                            ->get()
                            ->take(10000); 
        } else if ($request->query('source') == 'es') {
            $employee = ElasticsearchHelper::getEmployees(); 
        } else {
            $allEmployees = Redis::keys('employees:*');
            $count = 0;
            foreach ($allEmployees as $employeeNo) {
                $employee[] = json_decode(Redis::get($employeeNo));
                $count++;
                if ($count == 10000) {
                    break;
                }
            }
        }
        $end = Carbon::now()->getTimestampMs();
        return response()->json(['excute_time(ms)' => $end - $start, 'data' => $employee]);
    }

}
