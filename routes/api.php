<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\DepartmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/employees', [EmployeeController::class, 'showAll']);
Route::get('/employees/{empNo}', [EmployeeController::class, 'show']);
Route::get('/departments/{deptNo}/employees', [DepartmentController::class, 'listEmployees']);

