# laravel-data-process
The project offers different method of getting data. I design some apis to get data from mysql, elasticsearch or redis and caculate excution time. The project also show how to creat data structure and sync data to mysql, elasticsearch and redis automatically.

## Getting started

The project need mysql, elasticsearch and redis. Please install!
```
mac
brew install mysql
brew install elasticsearch
brew install redis
```

## Getting data

The project use [datacharmer/test_db](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) as data structure and data source. So please clone this project first.

```
git clone git@github.com:datacharmer/test_db.git
```

## Getting and setting project

```
git clone git@gitlab.com:hjoruhjoru/laravel-data-process.git
```
create database sql file folder for importing data
```
cd laravel-data-process
mkdir database/sql_file
```

copy the following dump file from test_db and put those file to laravel-data-process/database/sql_file
```
load_departments.dump
load_dept_emp.dump
load_dept_manager.dump
load_employees.dump
load_salaries1.dump
load_salaries2.dump
load_salaries3.dump
load_titles.dump
```

## Running migration file to create database table

```
php artisan migrate
```

## Running seeder to import data to mysql, elasticsearch and redis

```
php artisan db:seed EmployeeSeeder
php artisan db:seed EmployeeElasticsearchSeeder
php artisan db:seed EmployeeCacheSeeder
```

## Let's start server now!!

```
php artisan serve
```
